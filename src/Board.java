
public class Board {
	private char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' }, };

	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int turncount;

	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		current = x;
		winner = null;
		turncount = 0;
	}

	public char[][] getTable() {
		return table;
	}

	public Player getCurrent() {
		return current;

	}

	public boolean setTable(int R, int C) {
		if (table[R][C] == '-') {
			table[R][C] = current.getName();
			return true;
		}
		return false;
	}

	private boolean checkRow(int R) {
		for (int C = 0; C < table.length; C++) {
			if (table[R][C] != current.getName()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkRow() {
		if (checkRow(0) || checkRow(1) || checkRow(2)) {
			return true;
		}
		return false;
	}
	private boolean checkCol(int C) {
		for(int R=0;R<table.length;R++) {
			if(table[R][C]!=current.getName()) {
				return false;
			}
		}
		return true;
		}
	
	private boolean checkCol() {
		if(checkCol(0)||checkCol(1)||checkCol(2)) {
			return true;
		}
		return false;
	}
		private boolean checkx1() {
			for(int i = 0; i<table.length; i++) {
				if(table[i][i]!= current.getName()) {
					return false;
				}
			}
			return true;
		}
		
		private boolean checkx2() {
			for(int i = 0; i<table.length; i++) {
				if(table[2-i][i]!= current.getName()) {
					return false;
				}
			}
			return true;
		}
		
		private boolean checkDraw() {
			if(turncount == 8) {
				x.draw();
				o.draw();
				return true;
			}
			return false;
		}
		
		public boolean checkWin() {
			if(checkRow() || checkCol() || checkx1() || checkx2()) {
				winner =current;
				if(current == x) {
					x.win();
					o.lose();
				}else {
					o.win();
					x.lose();
				}
				return true;
			}
			return false;
			
		}
		
		public boolean isFinish() {
			if(checkWin()) {
				return true;
			}
			if(checkDraw()) {
				return true;
			}
			
			return false;
		}
		
		public void switchPlayer() {
			if(current == x) {
				current = o;
			}else {
				current = x;
			}
			turncount++;
		}
		public Player getWinner() {
			return winner;
		}

	}

