import java.util.Scanner;

public class Game {
	Scanner kb = new Scanner(System.in);
	private Board board;
	private Player x;
	private Player o;

	public Game() {

		o = new Player('O');
		x = new Player('X');
		
	}

	public void play() {
		while(true) {
			playOne();
		}
	}
	
	public void playOne() {
		board = new Board(x, o);
		 showWelocome();
		 while(true) {
			 showTable();
			 showTurn();
			 input();
			 if(board.isFinish()) {
				 break;
			 }
			 board.switchPlayer();
		 }
		 showTable();
		 showWinner();
		 showStat();
	}
	
	private void showStat() {
		System.out.println(x.getName() + " "+ "Win,Draw,Lose: ");
		System.out.println("  "+x.getWin() + ","+ x.getDraw()+","+x.getLose());
		
		System.out.println(o.getName()+" "+"Win,Draw,Lose: ");
		System.out.println("  "+o.getWin() + ","+ o.getDraw()+","+o.getLose());
	}
	private void showWinner() {
		Player player = board.getWinner();
		System.out.println(player.getName()+" You Win!!!");
	}

	private void showWelocome() {
		System.out.println("Welcome to OX Game.");
	}

	private void showTable() {
		char[][] table = board.getTable();
		System.out.println(" 1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print((i + 1));
			for (int j = 0; j < table.length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private void showTurn() {
		System.out.println(board.getCurrent().getName() + " turn: ");
	}

	private void input() {
		Scanner kb = new Scanner(System.in);
		
		int R;
		int C;
		while (true) {
			try {
			System.out.println("Please Input Row,Column : ");
			String input = kb.nextLine();
			String[] s = input.split(" ");
			if (s.length!=2) {
				System.out.println("Error, Please Input again");
				continue;
			} 
			
			R = Integer.parseInt(s[0])-1;
			C= Integer.parseInt(s[1])-1;
		 if (board.setTable(R,C)==false) {
				System.out.println("Table is not empty!");
				continue;

			} break;
			
			}catch(Exception e) {
				System.out.println("Error, Please Input again");
				continue;
			}
		}
		
	

			

		
	}

}
